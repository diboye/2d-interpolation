void FitM4lHisto()
  
{
  c1->Divide(3,3);
  gStyle->SetOptFit(1);
  gStyle->SetOptStat(1);
  for (int j = 0; j < 9; j++)
    {
      if (j==6) continue; // to remove the mass point mZd = 100; mS= 400 GeV
      c1->cd(j+1);
      hist_m4l_nom[j][channel]->SetTitle(TString::Format("%s_%s",l_name[j], i_name[channel]));
      TF1 *fitDSCB = new TF1("fitDSCB",DoubleSidedCrystalballFunction, leftRangeFitM4l[j],rightRangeFitM4l[j], 7);
      
      if (channel == 3)
	{
	  if (j==0 || j== 1 || j==2 || j== 4 || j==5)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral(leftRangeFitM4l[j],rightRangeFitM4l[j]));
	    }
	  
	  else if (j==3 || j==6 || j== 7|| j==8)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else fitDSCB->SetParameters(2, 5, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral(leftRangeFitM4l[j],rightRangeFitM4l[j]));
	}
      
      else if(channel == 2)
	{
	  if(j==0)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(),  hist_m4l_nom[j][channel]->Integral(leftRangeFitM4l[j],rightRangeFitM4l[j]));
	    }
	  
	  else if(j==2)
	    {
	      fitDSCB->SetParameters(1, 1, 1, 4, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if(j==4)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	    }
	  else if (j ==3 || j== 7 || j==8)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), 1);
	    }
	  else if (j==5 || j== 6)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	    }
	  else fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	  }
      else if(channel == 1)
	{
	  if(j==4)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	    }
	  else if(j==2)
	    {
	      fitDSCB->SetParameters(1, 1, 1, 4, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral(leftRangeFitM4l[j],rightRangeFitM4l[j]));
	    }
	   else if(j==3)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 5, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if (j== 7 || j==8)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), 1);
	    }
	  else if (j==5 || j== 6)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	    }
	  else fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	}

      else if(channel == 0)
	{
	  
	  if(j==4)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	    }
	  else if(j==2)
	    {
	      fitDSCB->SetParameters(1, 1, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral(leftRangeFitM4l[j],rightRangeFitM4l[j]));
	    }

	   else if(j==3 || j==7)
	    {
	      fitDSCB->SetParameters(1, 2, 2, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if (j==5 || j==6)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	    }
	  else if (j==8)
	    {
	      fitDSCB->SetParameters(1, 4, 1, 4, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), 1);
	    }
	  else fitDSCB->SetParameters(1, 2, 1, 2, hist_m4l_nom[j][channel]->GetMean(), hist_m4l_nom[j][channel]->GetRMS(), hist_m4l_nom[j][channel]->Integral());
	  }

      fitDSCB->SetParNames ("alpha_{low}","alpha_{high}","n_{low}", "n_{high}", "mean", "sigma", "Norm");
      hist_m4l_nom[j][channel]->Fit(fitDSCB, "", "", leftRangeFitM4l[j],rightRangeFitM4l[j]);
   
      MeanM4l[j][channel] = fitDSCB->GetParameter(4);
      SigmaM4l[j][channel]= fitDSCB->GetParameter(5);
      //IntegralM4l[j][channel] = fit->Integral(leftRangeHistXM4l[j], rightRangeHistXM4l[j]);
      err_Mean_m4l[j][channel] = fitDSCB->GetParError(4);
      err_Sigma_m4l[j][channel] = fitDSCB->GetParError(5);
      //cout << "************Sigma M4l ***************" << IntegralM4l[j][channel] << endl;
      
      IntegralM4lReco[j][channel] = hist_m4l_nom[j][channel]->Integral();
      // IntegralM4lReco[j][channel] = hist_m4l_nom[j][channel]->GetEntries();
      hist_m4l_nom[j][channel]->GetXaxis()->SetRangeUser(leftRangeHistXM4l[j], rightRangeHistXM4l[j]); //this line need to be put after the fit
        maxFitResult = fitDSCB->GetMaximum();
      maxHistY = hist_m4l_nom[j][channel]->GetMaximum();
      
      if (channel ==0)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+1); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+1);
	}
	}

      else if (channel ==1)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.4*maxFitResult); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.4*maxHistY);
	}
	}
       else if (channel ==2)
	{
      if (maxFitResult >= maxHistY)
		{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+1); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+1);
	}
	}

      if (channel ==3)
	{
      if (maxFitResult >= maxHistY)
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+1); // add 100 for all channel, at reco level
	}
      else
	{
	  hist_m4l_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+1);
	}
	}
      
      hist_m4l_nom[j][channel]->Draw("func");
      thePad->Print(TString::Format("plots/hist_m4l_%s_%s.pdf",l_name[j], i_name[channel]));
      
      
    }
}

void graph_integral_mS()
{
  TMultiGraph  *mg_mS  = new TMultiGraph();
  Double_t mass_mS[5]  = {175,325,400,750,800};
  Double_t integral_mS[5]  = {IntegralM4l[0][channel],IntegralM4l[5][channel],IntegralM4l[2][channel],IntegralM4l[7][channel],IntegralM4l[8][channel]};
  TGraphErrors *gr_integral_mS = new TGraphErrors(5,mass_mS,integral_mS,0,0);
  gr_integral_mS->SetLineColor(kRed);
  gr_integral_mS->Draw("acp");
  mg_mS->Add(gr_integral_mS);
  Double_t integral_reco_mS[5]  = {IntegralM4lReco[0][channel],IntegralM4lReco[5][channel],IntegralM4lReco[2][channel],IntegralM4lReco[7][channel],IntegralM4lReco[8][channel]};
  TGraphErrors *gr_integral_reco_mS = new TGraphErrors(5,mass_mS,integral_reco_mS,0,0);
  gr_integral_reco_mS->SetTitle("");
  gr_integral_reco_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
  gr_integral_reco_mS->GetYaxis()->SetTitle("integral");
  gr_integral_reco_mS->SetLineColor(kBlue);
  mg_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
  mg_mS->GetYaxis()->SetTitle("integral");
  mg_mS->Add(gr_integral_reco_mS);
  int n = gr_integral_reco_mS->GetN();
  double* y = gr_integral_reco_mS->GetY();
  int locmax = TMath::LocMax(n,y);
  double tmax = y[locmax]*10000;  
  mg_mS->GetYaxis()->SetRangeUser(0.1, tmax);
   c1->SetLogy();
  mg_mS->Draw("acp");  
  auto legend1 = new TLegend(0.2,0.7,0.3,0.8);
  legend1->SetTextFont(42);
  legend1->SetTextSize(0.03);
  legend1->SetBorderSize(0);
  legend1->AddEntry(gr_integral_reco_mS,"Integral from reco","lf");
  legend1->AddEntry(gr_integral_mS,"Integral from fit","lf");
  legend1->Draw();
  thePad->Print(TString::Format("plots/hist_integral_mS_%s.pdf", i_name[channel]));
     for (int i =0; i<18; i++)
    {
      
      //IntOfRecoHist_mS[i] = std::round(gr_integral_mS->Eval(mS_gen[i]));
      IntOfRecoHist_mS[i] = std::round(gr_integral_reco_mS->Eval(mS_gen[i]));
    
    }
     /*  //cout << "IntOfRecoHist_mZd[0]" << gr_integral_mZd->Eval(70) << endl;
    cout << "count_reco_mZd[0] = " << IntegralM4lReco[0][channel] << endl;
    cout << "count_reco_mZd[2] = " << IntegralM4lReco[2][channel] << endl;
    cout << "count_reco_mZd[3] = " << IntegralM4lReco[3][channel] << endl;
    cout << "count_reco_mZd[5] = " << IntegralM4lReco[5][channel] << endl;
    cout << "count_reco_mZd[6] = " << IntegralM4lReco[6][channel] << endl;
    cout << "count_reco_mZd[7] = " << IntegralM4lReco[7][channel] << endl;
    cout << "count_reco_mZd[8] = " << IntegralM4lReco[8][channel] << endl;

    //cout << "IntOfRecoHist_mZd[0]" << gr_integral_reco_mZd->Eval(70) << endl;
    cout << "IntOfRecoHist_mS[0]" << IntOfRecoHist_mS[0] << endl;

    cout << "count_fit_mZd[0] = " << IntegralM4l[0][channel] << endl;
    cout << "count_fit_mZd[2] = " << IntegralM4l[2][channel] << endl;
    cout << "count_fit_mZd[3] = " << IntegralM4l[3][channel] << endl;
    cout << "count_fit_mZd[5] = " << IntegralM4l[5][channel] << endl;
    cout << "count_fit_mZd[6] = " << IntegralM4l[6][channel] << endl;
    cout << "count_fit_mZd[7] = " << IntegralM4l[7][channel] << endl;
    cout << "count_fit_mZd[8] = " << IntegralM4l[8][channel] << endl;*/

  }

void graph_sigma_mS()

{
  c1->SetLogy(0);
  Double_t mass_mS[5]  = {175,325,400,750,800};
  Double_t sigma_mS[5]  = {SigmaM4l[0][channel],SigmaM4l[5][channel],SigmaM4l[2][channel],SigmaM4l[7][channel],SigmaM4l[8][channel]};
  Double_t sigma_error_mS[5] = {err_Sigma_m4l[0][channel],err_Sigma_m4l[5][channel],err_Sigma_m4l[2][channel],err_Sigma_m4l[7][channel],err_Sigma_m4l[8][channel]};
  TGraphErrors *gr_sigma_mS = new TGraphErrors(5,mass_mS,sigma_mS,0,sigma_error_mS);
  TSpline3 *s_mS = new TSpline3("grmS",gr_sigma_mS);
  gr_sigma_mS->SetTitle("");
  gr_sigma_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
  gr_sigma_mS->GetYaxis()->SetTitle("Fit param #sigma");
  // gr_sigma_mS->Fit("expo","","",175,800);
   gr_sigma_mS->Fit("pol2","","",150,810);
   gr_sigma_mS->GetXaxis()->SetRangeUser(0, 900);
   gr_sigma_mS->GetYaxis()->SetRangeUser(0, 200);
  gr_sigma_mS->Draw("ape");
  gr_sigma_mS->SetMarkerSize(1);
   gr_sigma_mS->SetMarkerStyle(8);
  s_mS->SetLineColor(kRed);
  thePad->Print(TString::Format("plots/hist_sigma_mS_%s.pdf", i_name[channel]));
      for (int i =0; i<18; i++)
    {
      
      //sigma_gen_mS[i] = gr_sigma_mS->GetFunction("expo")->Eval(mS_gen[i]);
      sigma_gen_mS[i] = gr_sigma_mS->GetFunction("pol2")->Eval(mS_gen[i]);
      cout << "*******sigma_mS[0]****** = " << sigma_gen_mS[0] << endl;

    }
     
}
void graph_mean_mS()
{
Double_t mass_mS[5]  = {175,325,400,750,800};
   Double_t mean_mS[5]  = {MeanM4l[0][channel],MeanM4l[5][channel],MeanM4l[2][channel],MeanM4l[7][channel],MeanM4l[8][channel]};
  Double_t mean_error_mS[5] = {err_Mean_m4l[0][channel],err_Mean_m4l[5][channel],err_Mean_m4l[2][channel],err_Mean_m4l[7][channel],err_Mean_m4l[8][channel]};
  TGraphErrors *gr_mean_mS = new TGraphErrors(5,mass_mS,mean_mS,0,mean_error_mS);
  gr_mean_mS->SetTitle("");
  gr_mean_mS->GetXaxis()->SetTitle("m_{S} [GeV]");
  gr_mean_mS->GetYaxis()->SetTitle("Fit param #mu");
  // c1->cd(2);
  gr_mean_mS->Fit("pol2","","",150,810);
  gr_mean_mS->GetXaxis()->SetRangeUser(0, 9000);
  gr_mean_mS->GetYaxis()->SetRangeUser(0, 1500);
  gr_mean_mS->Draw("APE");
    gr_mean_mS->SetMarkerSize(1);
   gr_mean_mS->SetMarkerStyle(8);
  //  s_mS->SetLineColor(kRed);
  thePad->Print(TString::Format("plots/hist_mean_mS_%s.pdf", i_name[channel]));
      for (int i =0; i<18; i++)
    {
      
           mean_gen_mS[i]  = gr_mean_mS->GetFunction("pol2")->Eval(mS_gen[i]);
	   cout << "*********mean_gen_mS[0]******** =  " << mean_gen_mS[0] << endl;
     
    }
}

void Generating_mS_hist()
{
   for (int i_gen = 0; i_gen < 18; i_gen++)
	    {
	      
	      rand_DSCB_mS->SetParameters(1.5, 1.5, 1.5, 1.5, mean_gen_mS[i_gen], sigma_gen_mS[i_gen], 1);
	      hGen_mS[i_gen][channel]->FillRandom("rand_DSCB_mS", IntOfRecoHist_mS[i_gen]);
	    }

  /* for (int i_gen = 0; i_gen < 18; i_gen++)
	{
	  for (int j = 0; j < IntOfRecoHist_mS[i_gen]; j++)
	  //for (int j = 0; j < 281; j++)
	   
	    {
	     
	      rand_DSCB_mS->SetParameters(1, 1, 1, 1, mean_gen_mS[i_gen], sigma_gen_mS[i_gen], 1);
	      M_S_j = rand_DSCB_mS->GetRandom();
	      hGen_mS[i_gen][channel]->Fill(M_S_j);
	    }
	}*/

  
}


void compare_msGen_msReco()

{
  c1->SetLogy(0);
  //  cout << "integral_nom__mS" <<" " << hist_m4l_nom[4][channel]->Integral() <<endl;
  //cout << "IntOfGenHist_mS[i_gen]" <<" " << IntOfGenHist_mS[0] <<endl;
  //double scale250 = 1/hist_m4l_nom[4][channel]->Integral();
  //hist_m4l_nom[4][channel]->Scale(scale250);
  //double scale1 = 1/hGen_mS[0][channel]->Integral();
  //hGen_mS[0][channel]->Scale(0.01);
  //hGen_mS[0][channel]->Scale(scale250);
  hGen_mS[0][channel]->GetXaxis()->SetRangeUser(0,500);
  hist_m4l_nom[4][channel]->GetXaxis()->SetRangeUser(0,500);
   hGen_mS[0][channel]->SetLineColor(2);
  hist_m4l_nom[4][channel]->SetLineColor(4);
  hGen_mS[0][channel]->GetYaxis()->SetTitle("events/[2.5 GeV]");
 
  hGen_mS[0][channel]->Draw("hist");
   hist_m4l_nom[4][channel]->Draw("hist same");
 
 
  gStyle->SetOptFit(0);
  gStyle->SetOptStat(0);
  auto legend = new TLegend(0.2,0.5,0.3,0.8);
  legend->SetTextFont(42);
  legend->SetTextSize(0.03);
  legend->SetBorderSize(0);
  legend->AddEntry(hist_m4l_nom[4][channel],"reco signal","lf");
  legend->AddEntry(hGen_mS[0][channel],"Generated signal","lf");
  //legend->SetX1NDC(.7); //new x start position
  //legend->SetX2NDC(.5);
  legend->Draw();
  thePad->Print(TString::Format("plots/hGen_mS_250_%s.pdf", i_name[channel]));
  c1->Clear();
}
