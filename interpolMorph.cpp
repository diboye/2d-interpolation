#include <TStyle.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>
#include <TNtuple.h>
#include <TSystem.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TFitResult.h>
#include <TObject.h>
#include <TTree.h>
#include <TGraph.h>
#include <TColor.h>
#include <TLegend.h>
#include <TSpectrum.h>
#include <TLatex.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string.h>
#include <cmath>
#include <iomanip>
#include "RooWorkspace.h"
#include <RooRealVar.h>
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooAbsArg.h"
#include "RooMomentMorph.h"
#include "RooFit.h"
#include "TRandom3.h"
#include "TGraphErrors.h"
#include "TGaxis.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TSpline.h"
#include "TPaveStats.h"
#include "TExec.h"
#include "TPaletteAxis.h"
#include "declarationFile.h"
#include "function2DHist.h"
#include "functionForDarkHiggs.h"
#include "functionForDarkBoson.h"
using namespace std;
using namespace RooFit;



int main(int argc, char **argv)
{
  //TString option = GetOption();
   //TString file = GetOption();
  TString option;
    TString newfile;
if (newfile.IsNull()) newfile="signal.root";
  else {
    Ssiz_t pos = newfile.Last('.');
    if (pos<0) pos=newfile.Length();
    newfile.Replace(pos,20,".root");
  }
   fOut = TFile::Open(newfile, "RECREATE");


  /*********************************intialize the generated histograms with their different channels *************************/
  for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 18; j++)
	{
	  hGen_mZd[j][i] = new TH1D(TString::Format("hGen_mZd_%s_%s", i_name[i], c_mZd[j]), ";m_{Zd} GeV;events/[1.0 GeV]", 300, 0, 300);
	  hGen_mS[j][i] = new TH1D(TString::Format("hGen_mS_%s_%s", i_name[i], c_mS[j]),";m_{S} GeV;events/[2.5 GeV]", 552,120,1500);
	  
	}
    }
  

  /*************************************** vizialize a 2D plot of ms vs mZd***********************/
  Hist2D_gen_reco();
  c1->Clear();

  /******************************************** Reading the inputs********************************/
  //for nominal and weighted systematics 
  if (myfile.is_open())
    {
      while ( getline (myfile,line) )
	{
	  if (line.find("m4l") != string::npos)
	    {
	      lineCointainerM4l.push_back(line);
	    }
	  else lineCointainerAvgM.push_back(line);
	  
	}
      myfile.close();
    }


  //for kinematic systematics
  if (myfileKinSyst.is_open())
    {
      while ( getline (myfileKinSyst,line) )
	{
	  if (line.find("EG_RESOLUTION_ALL1up") != string::npos)
	    {
	      lineCointainerEG_RESOLUTION_ALL1[0].push_back(line);
	      lineCointainerAvgMkSyst[0][0].push_back(line);
	    }
	  else if (line.find("EG_RESOLUTION_ALL1down") != string::npos)
	    {
	      lineCointainerEG_RESOLUTION_ALL1[1].push_back(line);
	      lineCointainerAvgMkSyst[0][1].push_back(line);
	    }
	  else if (line.find("EG_SCALE_ALL1up") != string::npos)
	    {
	      lineCointainerEG_SCALE_ALL1[0].push_back(line);
	      lineCointainerAvgMkSyst[1][0].push_back(line);
	    }
	  else if (line.find("EG_SCALE_ALL1down") != string::npos)
	    {
	      lineCointainerEG_SCALE_ALL1[1].push_back(line);
	      lineCointainerAvgMkSyst[1][1].push_back(line);
	    }

	  else if (line.find("MUONS_ID1up") != string::npos)
	    {
	      lineCointainerMUONS_ID1[0].push_back(line);
	      lineCointainerAvgMkSyst[2][0].push_back(line);
	    }
	  else if (line.find("MUONS_ID1down") != string::npos)
	    {
	      lineCointainerMUONS_ID1[1].push_back(line);
	      lineCointainerAvgMkSyst[2][1].push_back(line);
	    }

	   else if (line.find("MUONS_MS1up") != string::npos)
	    {
	      lineCointainerMUONS_MS1[0].push_back(line);
	      lineCointainerAvgMkSyst[3][0].push_back(line);
	    }
	  else if (line.find("MUONS_MS1down") != string::npos)
	    {
	      lineCointainerMUONS_MS1[1].push_back(line);
	      lineCointainerAvgMkSyst[3][1].push_back(line);
	    }
	  else if (line.find("MUONS_SAGITTA_RESBIAS1up") != string::npos)
	    {
	      lineCointainerMUONS_SAGITTA_RESBIAS1[0].push_back(line);
	      lineCointainerAvgMkSyst[4][0].push_back(line);
	    }
	  else if (line.find("MUONS_SAGITTA_RESBIAS1down") != string::npos)
	    {
	      lineCointainerMUONS_SAGITTA_RESBIAS1[1].push_back(line);
	      lineCointainerAvgMkSyst[4][1].push_back(line);
	    }
	  else if (line.find("MUONS_SAGITTA_RHO1up") != string::npos)
	    {
	      lineCointainerMUONS_SAGITTA_RHO1[0].push_back(line);
	      lineCointainerAvgMkSyst[5][0].push_back(line);
	    }
	  else if (line.find("MUONS_SAGITTA_RHO1down") != string::npos)
	    {
	      lineCointainerMUONS_SAGITTA_RHO1[1].push_back(line);
	      lineCointainerAvgMkSyst[5][1].push_back(line);
	    }
	   else if (line.find("MUONS_SCALE1up") != string::npos)
	    {
	      lineCointainerMUONS_SCALE1[0].push_back(line);
	      lineCointainerAvgMkSyst[6][0].push_back(line);
	    }
	  else if (line.find("MUONS_SCALE1down") != string::npos)
	    {
	      lineCointainerMUONS_SCALE1[1].push_back(line);
	      lineCointainerAvgMkSyst[6][1].push_back(line);
	    }
	  
	}
      myfileKinSyst.close();
    }
 
  // chekcing if the file are well read 
      cout << " lines : " << lineCointainerAvgMkSyst[0][0][0] << '\n';
      cout << " lines : " << lineCointainerAvgMkSyst[0][0][1] << '\n';
      cout << " lines : " << lineCointainerAvgMkSyst[0][0][0] << '\n';
    
      /********************************** extracting the m4l histograms ******************/
      
      for (channel = 3; channel < 4; channel++)
	{
	  /***/  initializeVar(); /**/ //initializing the variables used in the main
	  for (int j = 0; j < 9; j++)
	    {
	      file_m4l[j] = TFile::Open(lineCointainerM4l[j].c_str());
	      hist_m4l_nom[j][channel]  = (TH1D*)file_m4l[j]->Get(TString::Format("havgM_%s", i_name[channel]));
	      
	    }
	  /**********************
           fit the m4l histograms, graph the integral, the mean and sigma then
           Generate the mS mass points with a DBSC
           comparing the reconstructed and the generated mass point for  mS = 250 GeV
	  **********************/
 
	  FitM4lHisto();
	  c1->Clear();
	  graph_integral_mS();
	  c1->Clear();
	  graph_sigma_mS();
	  c1->Clear();
	  graph_mean_mS();
	  c1->Clear();
	  Generating_mS_hist();
	  compare_msGen_msReco();
	  
	  /*******************closing m4l file to avoid such error like too many files oppened*************/
	  for (int j = 0; j < 9; j++)
	    {
	      file_m4l[j]->Close();
	    }
	  /******************* extracting the  average di lepton histogram************************/
	  for (i_Nwsyst = 0; i_Nwsyst < 1; i_Nwsyst++)
	    {
	      if (i_Nwsyst==0) // i_Nwsyst ==0 considers the nominal histograms
		{
		  for (int j = 0; j < 9; j++)
		    {
		      file[j] = TFile::Open(lineCointainerAvgM[j].c_str());
		      hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s",i_name[channel]));
		    }
		  /**********************
                  fit the <mll> histograms, graph the integral, the mean and sigma then
                  Generate the mS mass points with a DBSC and then
                  comparing the reconstructed and the generated mass point for  mZd = 70 GeV
                  Finaly the output are saved in directories following the a structed needed to perform the limit setting.
	          **********************/
	
		  FitAvgMllHisto();
		  c1->Clear();
		  graph_integral_mZd();
		  c1->Clear();
		  graph_sigma_mZd();
		  c1->Clear();
		  graph_mean_mZd();
		  c1->Clear();
		  Generating_mZd_hist();
		  compare_mZdGen_mZdReco();
		  SaveInNomDirect();
		}
	      
	      else // // i_Nwsyst !=0 considers the weighted systematics
		{ 
		  for (i_var = 0; i_var < 2; i_var++) // considers up (for i_var ==0) and down (for i_var ==1) systematics 
		    {
		      /************************reset the histograms to avoid double counting events*******************/
		      for (int i_gen = 0; i_gen < 18; i_gen++)
			{
			  hGen_mZd[i_gen][channel]->Reset();
			}
		      /******************* extracting the  average di lepton histogram for the weighted histogram************************/
		      for (int j = 0; j < 9; j++)
			{
			  file[j] = TFile::Open(lineCointainerAvgM[j].c_str());
			  //hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s_%s_%s",c_wSyst[i_Nwsyst], c_vSyst[i_var], i_name[channel]));
			  hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s",i_name[channel])); // use nominal temporarily
			}
		      
		      c1->Clear();
		      FitAvgMllHisto();
		      c1->Clear();
		      graph_integral_mZd();
		      c1->Clear();
		      graph_sigma_mZd();
		      c1->Clear();
		      graph_mean_mZd();
		      c1->Clear();
		      Generating_mZd_hist();
		      SaveOtherSystDirect();
		      // closing  file to avoid such error like too many files oppenned
		      for (int j = 0; j < 9; j++)
			{
			  file[j]->Close();
			}
		    }
		}
	    }
	  
	  /******************* This block of code does the same as above but with the kinematic systematics********************/
	  
	  /* for (int k=0; k< 7; k++)
	    {
	      for (i_var =0; i_var<2; i_var++)
		{	  
		  for (int i_gen = 0; i_gen < 18; i_gen++)//reset the histograms to avoid double counting events
		    {
		      hGen_mZd[i_gen][channel]->Reset();
		    }
		  
		  for (int j = 0; j < 9; j++)
		    {
		      //file_kSyst[j][i_var] = new TFile(lineCointainerAvgMkSyst[k][i_var][j].c_str());
		      file[j] = new TFile(lineCointainerAvgM[j].c_str()); //use nominal temporarily
		      hist_avgM_nom[j][channel]  = (TH1D*)file[j]->Get(TString::Format("havgM_%s",i_name[channel]));//use nominal temporarily
		      //hist_avgM_nom[j][channel]  = (TH1D*)file_kSyst[j][i_var]->Get(TString::Format("havgM_%s",i_name[channel]));
		    }
		  
		  FitAvgMllHisto();
		  c1->Clear();
		  graph_integral_mZd();
		  c1->Clear();
		  graph_sigma_mZd();
		  c1->Clear();
		  graph_mean_mZd();
		  c1->Clear();
		  Generating_mZd_hist();
		 
		  for (int i_gen = 0; i_gen < 18; i_gen++)
		    {
		      file_saveFitHist =  new TFile(TString::Format("BRscaled_gaussiansignal_%schannel_%s.root",i_name[channel], c_mZdmS[i_gen]),"update");
		      if (i_var ==0)  myDirect = file_saveFitHist->mkdir(TString::Format("%s",c_kSyst[k]));
		      else file_saveFitHist->cd(TString::Format("%s",c_kSyst[k])); 
		      sig = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
		      sig->SetName(TString::Format("sig_%s",c_vSyst[i_var]));
		      file_saveFitHist->cd(TString::Format("%s",c_kSyst[k]));
		      sig->Write();
		      sig->Reset();
		      file_saveFitHist->Close();
		    }
		  for (int j = 0; j < 9; j++)
		    {
		      //file_kSyst[j][i_var]->Close();
		      file[j]->Close();//use nominal temporarily
		    }
		}
	    }*/
	   
	  for (int i_gen = 0; i_gen < 18; i_gen++)//reset the histograms to avoid double counting events
	    {
	      hGen_mZd[i_gen][channel]->Reset();
	    }
	}
    
  //TString signal = GetOption();
  TString signal;
  if (signal.IsNull()) {
    if (fOut && fOut->IsOpen()) {
      fOut->Write();
      fOut->Close();
    }
  } else {
    Ssiz_t pos = signal.Last('.');
    if (pos<0) pos=signal.Length();
    signal.Replace(pos,20,".pdf");
  }
  
  if (fOut && fOut->IsOpen()) {
    fOut->Write();
    fOut->Close();
  } 
  
  
  
  return 0;
 }
