//#include "declarationFile.h"

void Hist2D_gen_reco()

{

 TH2D *H2plane = new TH2D("H2plane","",20,0,300,20,0,1000);
  H2plane->Fill(20,175);
  H2plane->Fill(50,175);
  H2plane->Fill(50,400);
  H2plane->Fill(65,750);
  H2plane->Fill(70,250);
  H2plane->Fill(80,325);
  H2plane->Fill(100,400);
  H2plane->Fill(150,750);
  H2plane->Fill(250,800);
  H2plane->GetXaxis()->SetTitle("m_{Zd}");
  H2plane->GetZaxis()->SetTitle("ms");
  TH2D *H2planeGen = new TH2D("H2planeGen","",20,0,300,20,0,1000);
  H2planeGen->Fill(125,500);
  H2planeGen->Fill(130,600);
  H2planeGen->Fill(200,550);
  H2planeGen->Fill(135,300);
  H2planeGen->Fill(230,560);
  H2planeGen->Fill(260,650);
  H2planeGen->Fill(90,620);
  H2planeGen->Fill(95,700);
  H2planeGen->Fill(180,440);
  H2planeGen->Fill(160,380);
  H2planeGen->Fill(170,520);
  H2planeGen->Fill(60,510);
  H2planeGen->Fill(65,655);
  H2planeGen->Fill(165,615);
  H2planeGen->Fill(235,720);
  H2planeGen->Fill(185,680);
  H2planeGen->Fill(210,780);
  H2planeGen->GetXaxis()->SetTitle("m_{Zd}");
  H2planeGen->GetYaxis()->SetTitle("m_{S}");
  // TExec *ex_bkg = new TExec("ex_bkg","gStyle->SetPalette(kFruitPunch);");
  TExec *ex_sng = new TExec("ex_sng","gStyle->SetPalette(55);");
  gStyle->SetOptStat(0);
  H2plane->Draw("colz");
  gPad->Update();
  TPaletteAxis *palette = (TPaletteAxis*)H2plane->GetListOfFunctions()->FindObject("palette"); 
  palette->SetX1NDC(0.03);
  palette->SetX2NDC(-0.016);
  ex_sng->Draw();  
  H2planeGen->Draw("colz same");
  H2plane->GetZaxis()->SetLabelSize(0);
  gPad->Print("plots/mS_vs_mZd.pdf");
 
}



