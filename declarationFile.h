
using namespace std;
TH1D* hist_avgM_nom[9][4];
TH1D* hist_avgM_kSyst[9][4];
// Globals
TObject *MyObj = 0;

//TH1F* hist_avgM_nom[9][4];
TH1D* hist_m4l_nom[9][4];
double leftRangeFit[9];
double rightRangeFit[9];
double leftRangeFitM4l[9];
double rightRangeFitM4l[9];
double leftRangeHistXM4l[9];
double rightRangeHistXM4l[9];
double leftRangeHistX[9];
double rightRangeHistX[9];
double mZd[9];
double mS[9];
double sigma_el = 0.035;
double Mean[9][4];
double Sigma[9][4];
double Integral[9][4];
double IntegralReco[9][4];
double MeanM4l[9][4];
double SigmaM4l[9][4];
double IntegralM4l[9][4];
double IntegralM4lReco[9][4];
TFile* fOut;

double events_all[9];

double err_Mean[9][4];
double err_Sigma[9][4];
double err_Mean_m4l[9][4];
double err_Sigma_m4l[9][4];
const char *l_name[9] = {"signal_20_175", "signal_50_175","signal_50_400","signal_65_750","signal_70_250","signal_80_325","signal_100_400", "signal_150_750","signal_250_800"};
//TF1 *fit = new TF1("fit","crystalball");
/*Double_t VoigtFunction(Double_t*x, Double_t *par)//convolution of Gaussian and Lorent function
{
  par[0]=abs(par[0]);
  Double_t fitval = par[0]*TMath::Voigt(x[0]-par[1],par[2],par[3],4) + par[4];
  return fitval;
  }*/
double DoubleSidedCrystalballFunction(double *x, double *par)
 { 
   double alpha_l = par[0]; 
   double alpha_h = par[1]; 
   double n_l     = par[2]; 
   double n_h     = par[3]; 
   double mean	= par[4]; 
   double sigma	= par[5];
   double N	= par[6];
   double t = (x[0]-mean)/sigma;
   double result = -11111;
   double fact1TLessMinosAlphaL = alpha_l/n_l;
double fact2TLessMinosAlphaL = (n_l/alpha_l) - alpha_l -t;
   double fact1THihgerAlphaH = alpha_h/n_h;
   double fact2THigherAlphaH = (n_h/alpha_h) - alpha_h +t;
   
   if (-alpha_l <= t && alpha_h >= t)
     {
       result = exp(-0.5*t*t);
     }
   else if (t < -alpha_l)
     {
       
       result = exp(-0.5*alpha_l*alpha_l)*pow(fact1TLessMinosAlphaL*fact2TLessMinosAlphaL, -n_l);
       
     }
   else if (t > alpha_h)
     {
       result = exp(-0.5*alpha_h*alpha_h)*pow(fact1THihgerAlphaH*fact2THigherAlphaH, -n_h);
       
     }
   return N*result;
 }


TF1 *rand_DSCB_mS = new TF1("rand_DSCB_mS",DoubleSidedCrystalballFunction, 120, 1500, 7);
TF1 *rand_DSCB_mZd = new TF1("rand_DSCB_mZd",DoubleSidedCrystalballFunction, 0, 300, 7);



TFile* file[9];
TFile* file_m4l[9];
TFile* file_kSyst[9][2];
double maxFitResult=-111;
double maxHistY=-111;
const char *i_name[] = {"4e", "2e2m", "4m", "all"};
 string line;
//TString files[]={"./inputListFile.dat", "./inputListFileSyst.dat"};
//ifstream myfile;// (files[0]);
//ifstream myfile ("inputListFile.dat");//for reco
 ifstream myfile ("inputListFileFull.dat");//for reco
ifstream myfileKinSyst ("inputListFileFullSyst.dat");//for reco
 //ifstream myfile ("inputListFileTruth.dat");//for truth
vector<string> lineCointainerAvgM;
vector<string> lineCointainerAvgMkSyst[7][2];
vector<string> lineCointainerEG_RESOLUTION_ALL1[2];
vector<string> lineCointainerEG_SCALE_ALL1[2];
vector<string> lineCointainerMUONS_ID1[2];
vector<string> lineCointainerMUONS_MS1[2];
vector<string> lineCointainerMUONS_SAGITTA_RESBIAS1[2];
vector<string> lineCointainerMUONS_SAGITTA_RHO1[2];
vector<string> lineCointainerMUONS_SCALE1[2];
vector<string> lineCointainerM4l;
vector <vector<string>> vectorContainerkSyst;
string m4l;
string EG_RESOLUTION_ALL1up;
  Int_t IntOfRecoHist_mZd[18];
Int_t IntOfGenHist_mZd[18];
  double mean_gen_mZd[18];
  double sigma_gen_mZd[18];
  
  Int_t  IntOfRecoHist_mS[18];
Int_t  IntOfGenHist_mS[18];
  double mean_gen_mS[18];
  double sigma_gen_mS[18];
  
  double mS_gen[18];
  double mZd_gen[18];
  double QCD_scale_up[18][3];
  double QCD_scale_down[18][18];
  
  double PDF_scale_up[18][3];
  double PDF_scale_down[18][3];

  double QCD_uncert_up[18];
  double QCD_uncert_down[18];
  
  double PDF_uncert_up[18];
  double PDF_uncert_down[18];
    TFile* file_saveFitHist;
     TObjArray *MyHistArrayGen = new TObjArray(0);
TObjArray *MyHistArrayHistAll = new TObjArray(0);
    //TFile* file_saveGenHist =  new TFile("SavingGenHist.root", "recreate");
    //file_saveGenHist->Close();
    TFile *file_saveGenHist;
    TH1F *sig;
    TH1F *sig_up;
    TH1F *sig_down;
    TDirectory *myDirect;
    TH1D *hGen_mZd[18][4];
    TH1D *hGen_mS[18][4];
    int channel;
 TCanvas *c1 = new TCanvas("c1","c1",700,500);
      TPad* thePad = (TPad*)c1->cd();
Double_t M_Z_d_i, M_S_j;
int i_Nwsyst = 0;
int i_var;
//gROOT->ForceStyle();
//gStyle->SetOptStat(0);
//    gStyle->SetOptFit();


 const char *c_mZdmS[] =
      {
	"mZd70_mH250GeV",
	"mZd125_mH500GeV",
	"mZd130_mH600GeV",
	"mZd200_mH550GeV",
	"mZd135_mH300GeV",
	"mZd230_mH560GeV",
	"mZd260_mH650GeV",
	"mZd75_mH620GeV",
	"mZd110_mH700GeV",
	"mZd180_mH440GeV",
	"mZd160_mH380GeV",
	"mZd170_mH520GeV",
	"mZd60_mH510GeV",
	"mZd65_mH655GeV",
	"mZd165_mH615GeV",
	"mZd235_mH720GeV",
	"mZd185_mH680GeV",
	"mZd210_mH780GeV"
      };
    
    const char *c_mZd[] =
      {
	"mZd70",
	"mZd125",
	"mZd130",
	"mZd200",
	"mZd135",
	"mZd230",
	"mZd260",
	"mZd75",
	"mZd110",
	"mZd180",
	"mZd160",
	"mZd170",
	"mZd60",
	"mZd65",
	"mZd165",
	"mZd235",
	"mZd185",
	"mZd210"
      };


    
    const char *c_mS[] =
      {
	"mH250GeV",
	"mH500GeV",
	"mH600GeV",
	"mH550GeV",
	"mH300GeV",
	"mH560GeV",
	"mH650GeV",
	"mH620GeV",
	"mH700GeV",
	"mH440GeV",
	"mH380GeV",
	"mH520GeV",
	"mH510GeV",
	"mH655GeV",
	"mH615GeV",
	"mH720GeV",
	"mH680GeV",
	"mH780GeV"
      };
  
    const char *c_wSyst[] =
      {
	"EL_EFF_ID_TOTAL",
	"EL_EFF_ISO_TOTAL",
	"EL_EFF_RECO_TOTAL",
	"MUON_EFF_ISO_STAT",
	"MUON_EFF_ISO_SYS",
	"MUON_EFF_RECO_STAT",
	"MUON_EFF_RECO_STAT_LOWPT",
	"MUON_EFF_RECO_SYS",
	"MUON_EFF_RECO_SYS_LOWPT",
	"MUON_EFF_TTVA_STAT",
	"MUON_EFF_TTVA_SYS",
	"Pileup_weight"
      };

const char *c_kSyst[] =
      {
	"EG_RESOLUTION_ALL1",
	"EG_SCALE_ALL1",
	"MUONS_ID1",
	"MUONS_MS1",
	"MUONS_SAGITTA_RESBIAS1",
	"MUONS_SAGITTA_RHO1",
	"MUONS_SCALE1"

      };
    
    const char *c_vSyst[] =
      {
	"up",
	"down"
      };
   


void  initializeVar()
{

  mZd[0] = 20;
  mZd[1] = 50;
  mZd[2] = 50;
  mZd[3] = 65;
  mZd[4] = 70;
  mZd[5] = 80;
  mZd[6] = 100;
  mZd[7] = 150;
  mZd[8] = 250;



  mZd_gen[0]  = 70;
  mZd_gen[1]  = 125;
  mZd_gen[2]  = 130;
  mZd_gen[3]  = 200;
  mZd_gen[4]  = 135;
  mZd_gen[5]  = 230;
  mZd_gen[6]  = 260;
  mZd_gen[7]  = 75;
  mZd_gen[8]  = 110;
  mZd_gen[9]  = 180;
  mZd_gen[10] = 160;
  mZd_gen[11] = 170;
  mZd_gen[12] = 60;
  mZd_gen[13] = 65;
  mZd_gen[14] = 165;
  mZd_gen[15] = 235;
  mZd_gen[16] = 185;
  mZd_gen[17] = 210;
  
  mS_gen[0]  = 250;
  mS_gen[1]  = 500;
  mS_gen[2]  = 600;
  mS_gen[3]  = 550;
  mS_gen[4]  = 300;
  mS_gen[5]  = 560;
  mS_gen[6]  = 650;
  mS_gen[7]  = 620;
  mS_gen[8]  = 700;
  mS_gen[9]  = 440;
  mS_gen[10] = 380;
  mS_gen[11] = 520;
  mS_gen[12] = 510;
  mS_gen[13] = 655;
  mS_gen[14] = 615;
  mS_gen[15] = 720;
  mS_gen[16] = 680;
  mS_gen[17] = 780;




  
  QCD_uncert_up[0]  = 5.9/100;   //mH = 250
  QCD_uncert_up[1]  = 5.9/100;   //mH = 500
  QCD_uncert_up[2]  = 5.5/100;   //mH = 600
  QCD_uncert_up[3]  = 5.5/100;   //mH = 550
  QCD_uncert_up[4]  = 5.8/100;   //mH = 300
  QCD_uncert_up[5]  = 5.5/100;   //mH = 560
  QCD_uncert_up[6]  = 5.5/100;   //mH = 650
  QCD_uncert_up[7]  = 5.5/100;   //mH = 620
  QCD_uncert_up[8]  = 5.4/100;   //mH = 700
  QCD_uncert_up[9]  = 5.7/100;   //mH = 440
  QCD_uncert_up[10] = 5.7/100;   //mH = 380
  QCD_uncert_up[11] = 5.6/100;   //mH = 520
  QCD_uncert_up[12] = 5.6/100;   //mH = 510
  QCD_uncert_up[13] = 5.5/100;   //mH = 655
  QCD_uncert_up[14] = 5.5/100;   //mH = 615
  QCD_uncert_up[15] = 5.4/100;   //mH = 720
  QCD_uncert_up[16] = 5.4/100;   //mH = 680
  QCD_uncert_up[17] = 5.5/100;   //mH = 780
  
  
  QCD_uncert_down[0]  = 6.5/100;   //mH = 250
  QCD_uncert_down[1]  = 5.1/100;   //mH = 500
  QCD_uncert_down[2]  = 4.9/100;   //mH = 600
  QCD_uncert_down[3]  = 5.0/100;   //mH = 550
  QCD_uncert_down[4]  = 6.2/100;   //mH = 300
  QCD_uncert_down[5]  = 4.9/100;   //mH = 560
  QCD_uncert_down[6]  = 4.9/100;   //mH = 650
  QCD_uncert_down[7]  = 4.9/100;   //mH = 620
  QCD_uncert_down[8]  = 4.8/100;   //mH = 700
  QCD_uncert_down[9]  = 5.3/100;   //mH = 440
  QCD_uncert_down[10] = 5.7/100;   //mH = 380
  QCD_uncert_down[11] = 5.1/100;   //mH = 520
  QCD_uncert_down[12] = 5.1/100;   //mH = 510
  QCD_uncert_down[13] = 4.9/100;   //mH = 655
  QCD_uncert_down[14] = 5.5/100;   //mH = 615
  QCD_uncert_down[15] = 4.8/100;   //mH = 720
  QCD_uncert_down[16] = 4.8/100;   //mH = 680
  QCD_uncert_down[17] = 4.7/100;   //mH = 780
  
  
  PDF_uncert_up[0]  = 2.9/100;   //mH = 250
  PDF_uncert_up[1]  = 3.1/100;   //mH = 500
  PDF_uncert_up[2]  = 3.4/100;   //mH = 600
  PDF_uncert_up[3]  = 3.2/100;   //mH = 550
  PDF_uncert_up[4]  = 2.9/100;   //mH = 300
  PDF_uncert_up[5]  = 3.2/100;   //mH = 560
  PDF_uncert_up[6]  = 3.5/100;   //mH = 650
  PDF_uncert_up[7]  = 3.4/100;   //mH = 620
  PDF_uncert_up[8]  = 3.7/100;   //mH = 700
  PDF_uncert_up[9]  = 3.0/100;   //mH = 440
  PDF_uncert_up[10] = 2.9/100;   //mH = 380
  PDF_uncert_up[11] = 3.1/100;   //mH = 520
  PDF_uncert_up[12] = 3.1/100;   //mH = 510
  PDF_uncert_up[13] = 3.5/100;   //mH = 655
  PDF_uncert_up[14] = 3.4/100;   //mH = 615
  PDF_uncert_up[15] = 3.7/100;   //mH = 720
  PDF_uncert_up[16] = 3.7/100;   //mH = 680
  PDF_uncert_up[17] = 4.0/100;   //mH = 780
  
  
  PDF_uncert_down[0]  = 2.9/100;   //mH = 250
  PDF_uncert_down[1]  = 3.1/100;   //mH = 500
  PDF_uncert_down[2]  = 3.4/100;   //mH = 600
  PDF_uncert_down[3]  = 3.2/100;   //mH = 550
  PDF_uncert_down[4]  = 2.9/100;   //mH = 300
  PDF_uncert_down[5]  = 3.2/100;   //mH = 560
  PDF_uncert_down[6]  = 3.5/100;   //mH = 650
  PDF_uncert_down[7]  = 3.4/100;   //mH = 620
  PDF_uncert_down[8]  = 3.7/100;   //mH = 700
  PDF_uncert_down[9]  = 3.0/100;   //mH = 440
  PDF_uncert_down[10] = 2.9/100;   //mH = 380
  PDF_uncert_down[11] = 3.1/100;   //mH = 520
  PDF_uncert_down[12] = 3.1/100;   //mH = 510
  PDF_uncert_down[13] = 3.5/100;   //mH = 655
  PDF_uncert_down[14] = 3.4/100;   //mH = 615
  PDF_uncert_down[15] = 3.7/100;   //mH = 720
  PDF_uncert_down[16] = 3.7/100;   //mH = 680
  PDF_uncert_down[17] = 4.0/100;   //mH = 780
  
  
  mS[0] = 175;
  mS[1] = 175;
  mS[2] = 400;
  mS[3] = 750;
  mS[4] = 250;
  mS[5] = 325;
  mS[6] = 400;
  mS[7] = 750;
  mS[8] = 800;

  //TF1 *fit = new TF1("MyCrystalBall","crystalball",-5.,5.);

   if (channel == 0)
    { 
  leftRangeFit[0] = 17;
  leftRangeFit[1] = 46;
  leftRangeFit[2] = 45;
  leftRangeFit[3] = 60;
  leftRangeFit[4] = 64;
  leftRangeFit[5] = 75;
  leftRangeFit[6] = 96;
  leftRangeFit[7] = 135;
  leftRangeFit[8] = 235;

  rightRangeFit[0] = 22;
  rightRangeFit[1] = 54;
  rightRangeFit[2] = 53;
  rightRangeFit[3] = 68;
  rightRangeFit[4] = 75;
  rightRangeFit[5] = 83;
  rightRangeFit[6] = 106;
  rightRangeFit[7] = 155;
  rightRangeFit[8] = 260;

  leftRangeFitM4l[0] = 152;
  leftRangeFitM4l[1] = 150;
  leftRangeFitM4l[2] = 250;
  leftRangeFitM4l[3] = 400;
  leftRangeFitM4l[4] = 210;
  leftRangeFitM4l[5] = 250;
  leftRangeFitM4l[6] = 320;
  leftRangeFitM4l[7] = 400;
  leftRangeFitM4l[8] = 500;

  rightRangeFitM4l[0] = 190;
  rightRangeFitM4l[1] = 190;
  rightRangeFitM4l[2] = 550;
  rightRangeFitM4l[3] = 1400;
  rightRangeFitM4l[4] = 290;
  rightRangeFitM4l[5] = 400;
  rightRangeFitM4l[6] = 420;
  rightRangeFitM4l[7] = 1400;
  rightRangeFitM4l[8] = 1400;

  leftRangeHistX[0] = 10;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = 55;
  leftRangeHistX[4] = 60;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = 130;
  leftRangeHistX[8] = 230;

  rightRangeHistX[0] = 30;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = 75;
  rightRangeHistX[4] = 80;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = 170;
  rightRangeHistX[8] = 270;

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = 200;
  leftRangeHistXM4l[3] = 200;
  leftRangeHistXM4l[4] = 200;
  leftRangeHistXM4l[5] = 200;
  leftRangeHistXM4l[6] = 310;
  leftRangeHistXM4l[7] = 200;
  leftRangeHistXM4l[8] = 400;

  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = 550;
  rightRangeHistXM4l[3] = 1500;
  rightRangeHistXM4l[4] = 300;
  rightRangeHistXM4l[5] = 450;
  rightRangeHistXM4l[6] = 480;
  rightRangeHistXM4l[7] = 1500;
  rightRangeHistXM4l[8] = 1500;
 
    }

   else if (channel == 1)
    {
  leftRangeFit[0] = 16;
  leftRangeFit[1] = 46;
  leftRangeFit[2] = 44;
  leftRangeFit[3] = 58;
  leftRangeFit[4] = 64;
  leftRangeFit[5] = 72;
  leftRangeFit[6] = 96;
  leftRangeFit[7] = 130;
  leftRangeFit[8] = 230;

  rightRangeFit[0] = 22;
  rightRangeFit[1] = 54;
  rightRangeFit[2] = 54;
  rightRangeFit[3] = 70;
  rightRangeFit[4] = 74;
  rightRangeFit[5] = 83;
  rightRangeFit[6] = 106;
  rightRangeFit[7] = 170;
  rightRangeFit[8] = 265;

  leftRangeFitM4l[0] = 152;
  leftRangeFitM4l[1] = 150;
  leftRangeFitM4l[2] = 250;
  leftRangeFitM4l[3] = 300;
  leftRangeFitM4l[4] = 210;
  leftRangeFitM4l[5] = 250;
  leftRangeFitM4l[6] = 320;
  leftRangeFitM4l[7] = 300;
  leftRangeFitM4l[8] = 500;

  rightRangeFitM4l[0] = 190;
  rightRangeFitM4l[1] = 190;
  rightRangeFitM4l[2] = 550;
  rightRangeFitM4l[3] = 1400;
  rightRangeFitM4l[4] = 290;
  rightRangeFitM4l[5] = 400;
  rightRangeFitM4l[6] = 420;
  rightRangeFitM4l[7] = 1250;
  rightRangeFitM4l[8] = 1400;

  leftRangeHistX[0] = 10;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = 55;
  leftRangeHistX[4] = 60;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = 130;
  leftRangeHistX[8] = 230;

  rightRangeHistX[0] = 30;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = 75;
  rightRangeHistX[4] = 80;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = 170;
  rightRangeHistX[8] = 270;

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = 200;
  leftRangeHistXM4l[3] = 200;
  leftRangeHistXM4l[4] = 200;
  leftRangeHistXM4l[5] = 200;
  leftRangeHistXM4l[6] = 310;
  leftRangeHistXM4l[7] = 200;
  leftRangeHistXM4l[8] = 400;

  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = 550;
  rightRangeHistXM4l[3] = 1500;
  rightRangeHistXM4l[4] = 300;
  rightRangeHistXM4l[5] = 450;
  rightRangeHistXM4l[6] = 480;
  rightRangeHistXM4l[7] = 1500;
  rightRangeHistXM4l[8] = 1500;
    }

    else if (channel == 2)
    {
  leftRangeFit[0] = 17;
  leftRangeFit[1] = 47;
  leftRangeFit[2] = 46;
  leftRangeFit[3] = 57;
  leftRangeFit[4] = 65;
  leftRangeFit[5] = 73;
  leftRangeFit[6] = 96;
  leftRangeFit[7] = 130;
  leftRangeFit[8] = 230;

  rightRangeFit[0] = 22;
  rightRangeFit[1] = 53;
  rightRangeFit[2] = 53;
  rightRangeFit[3] = 71;
  rightRangeFit[4] = 74;
  rightRangeFit[5] = 83;
  rightRangeFit[6] = 106;
  rightRangeFit[7] = 170;
  rightRangeFit[8] = 270;

  leftRangeFitM4l[0] = 152;
  leftRangeFitM4l[1] = 150;
  leftRangeFitM4l[2] = 250;
  leftRangeFitM4l[3] = 300;
  leftRangeFitM4l[4] = 210;
  leftRangeFitM4l[5] = 250;
  leftRangeFitM4l[6] = 320;
  leftRangeFitM4l[7] = 300;
  leftRangeFitM4l[8] = 500;

  rightRangeFitM4l[0] = 190;
  rightRangeFitM4l[1] = 190;
  rightRangeFitM4l[2] = 550;
  rightRangeFitM4l[3] = 1400;
  rightRangeFitM4l[4] = 290;
  rightRangeFitM4l[5] = 400;
  rightRangeFitM4l[6] = 420;
  rightRangeFitM4l[7] = 1250;
  rightRangeFitM4l[8] = 1400;

  leftRangeHistX[0] = 10;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = 55;
  leftRangeHistX[4] = 60;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = 130;
  leftRangeHistX[8] = 230;

  rightRangeHistX[0] = 30;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = 75;
  rightRangeHistX[4] = 80;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = 170;
  rightRangeHistX[8] = 270;

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = 200;
  leftRangeHistXM4l[3] = 200;
  leftRangeHistXM4l[4] = 200;
  leftRangeHistXM4l[5] = 200;
  leftRangeHistXM4l[6] = 310;
  leftRangeHistXM4l[7] = 200;
  leftRangeHistXM4l[8] = 400;

  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = 550;
  rightRangeHistXM4l[3] = 1500;
  rightRangeHistXM4l[4] = 300;
  rightRangeHistXM4l[5] = 450;
  rightRangeHistXM4l[6] = 480;
  rightRangeHistXM4l[7] = 1500;
  rightRangeHistXM4l[8] = 1500;
 
    }


   else if (channel == 3)
    {
  leftRangeFit[0] = 18;
  leftRangeFit[1] = 46;
  leftRangeFit[2] = 45;
  leftRangeFit[3] = 58;
  leftRangeFit[4] = 64;
  leftRangeFit[5] = 71;
  leftRangeFit[6] = 96;
  leftRangeFit[7] = 130;
  leftRangeFit[8] = 230;

  rightRangeFit[0] = 22;
  rightRangeFit[1] = 54;
  rightRangeFit[2] = 54;
  rightRangeFit[3] = 72;
  rightRangeFit[4] = 75;
  rightRangeFit[5] = 83;
  rightRangeFit[6] = 106;
  rightRangeFit[7] = 170;
  rightRangeFit[8] = 270;

  leftRangeFitM4l[0] = 150;
  leftRangeFitM4l[1] = 150;
  leftRangeFitM4l[2] = 250;
  leftRangeFitM4l[3] = 300;
  leftRangeFitM4l[4] = 210;
  leftRangeFitM4l[5] = 250;
  leftRangeFitM4l[6] = 320;
  leftRangeFitM4l[7] = 300;
  leftRangeFitM4l[8] = 500;

  rightRangeFitM4l[0] = 190;
  rightRangeFitM4l[1] = 190;
  rightRangeFitM4l[2] = 500;
  rightRangeFitM4l[3] = 1400;
  rightRangeFitM4l[4] = 290;
  rightRangeFitM4l[5] = 400;
  rightRangeFitM4l[6] = 420;
  rightRangeFitM4l[7] = 1250;
  rightRangeFitM4l[8] = 1400;

  leftRangeHistX[0] = 10;
  leftRangeHistX[1] = mZd[1]-10;
  leftRangeHistX[2] = mZd[2]-10;
  leftRangeHistX[3] = 55;
  leftRangeHistX[4] = 60;
  leftRangeHistX[5] = mZd[5]-10;
  leftRangeHistX[6] = mZd[6]-10;
  leftRangeHistX[7] = 130;
  leftRangeHistX[8] = 230;

  rightRangeHistX[0] = 30;
  rightRangeHistX[1] = mZd[1]+10;
  rightRangeHistX[2] = mZd[2]+10;
  rightRangeHistX[3] = 75;
  rightRangeHistX[4] = 80;
  rightRangeHistX[5] = mZd[5]+10;
  rightRangeHistX[6] = mZd[6]+10;
  rightRangeHistX[7] = 170;
  rightRangeHistX[8] = 270;

  leftRangeHistXM4l[0] = mS[0]-40;
  leftRangeHistXM4l[1] = mS[1]-40;
  leftRangeHistXM4l[2] = 200;
  leftRangeHistXM4l[3] = 200;
  leftRangeHistXM4l[4] = 200;
  leftRangeHistXM4l[5] = 200;
  leftRangeHistXM4l[6] = 310;
  leftRangeHistXM4l[7] = 200;
  leftRangeHistXM4l[8] = 400;

  rightRangeHistXM4l[0] = mS[0]+40;
  rightRangeHistXM4l[1] = mS[1]+40;
  rightRangeHistXM4l[2] = 550;
  rightRangeHistXM4l[3] = 1500;
  rightRangeHistXM4l[4] = 300;
  rightRangeHistXM4l[5] = 450;
  rightRangeHistXM4l[6] = 480;
  rightRangeHistXM4l[7] = 1500;
  rightRangeHistXM4l[8] = 1500;
    }


}
