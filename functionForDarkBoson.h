void FitAvgMllHisto()

{
  c1->Divide(3,3);
  gStyle->SetOptFit(1);
  gStyle->SetOptStat(1);
  
  for (int j = 0; j < 9; j++)
    {
      if (j==6) continue; // to remove the mass point mZd = 100; mS= 400 GeV
       c1->cd(j+1);
     
      hist_avgM_nom[j][channel]->SetTitle(TString::Format("%s_%s",l_name[j],i_name[channel]));
      
      TF1 *fitDSCB = new TF1("fitDSCB",DoubleSidedCrystalballFunction, leftRangeFit[j],rightRangeFit[j], 7);
      
      if (channel == 3)
	{

	  if (j==0)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	 else  if (j==1 || j==7)
	    {
	      fitDSCB->SetParameters(1, 1, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  
	  else if (j==2)
	    {
	      
	      fitDSCB->SetParameters(1, 3, 2, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  else if (j==3)
	    {
	      
	      fitDSCB->SetParameters(1, 1, 1, 3, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),  hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  else if (j==4)
	    {
	      
	      fitDSCB->SetParameters(1, 4, 4, 5, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),  1);
	    }
	  
	  else if (j==5)
	    {
	      
	      fitDSCB->SetParameters(1, 2, 2, 1, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),  1);
	    }

	  else if (j==8)
	    {
	      
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),   hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  else
	    {
	      
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	}
      else if (channel ==2)
	{
	  
	  if (j==0)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if (j==3)
	    {
	      fitDSCB->SetParameters(1, 3, 7, 5, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if (j==1)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 4, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),1);
	    }
	  
	  else if (j==7 || j==8)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  
	  else if (j==2)
	    {
	      
	      fitDSCB->SetParameters(1, 2, 2, 5, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if (j==4)
	    {
	      
	      fitDSCB->SetParameters(1, 1, 1, 3, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),  1);
	    }
	  
	  else if (j==5)
	    {
	      
	      fitDSCB->SetParameters(1, 2, 1, 3, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),  1);
	    }
	  else
	    {
	      
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	}
      else if (channel ==1)
	{
	  
	  if (j==0)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),  hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  
	  else if (j==1)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  
	  else if (j==7 || j==8)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  
	  else if (j==2)
	    {
	      
	      fitDSCB->SetParameters(1, 2, 1, 3, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(),  hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  
	  else if (j==4 || j==3)
	    {
	      
	      fitDSCB->SetParameters(1, 2, 1, 4, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  
	  else if (j==5)
	    {
	      
	      fitDSCB->SetParameters(1, 2, 5, 1, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), hist_avgM_nom[j][channel]->Integral(leftRangeFit[j],rightRangeFit[j]));
	    }
	  else
	    {
	      
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	}
      
      else if (channel ==0)
	{
	  
	  if (j==0 || j==6)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if (j==1)
	    {
	      fitDSCB->SetParameters(1, 1, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  else if (j==3)
	    {
	      fitDSCB->SetParameters(1, 1, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }

	  else if (j==4)
	    {
	      fitDSCB->SetParameters(2, 4, 2, 4, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  else if (j==7)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 7, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }

	  else if (j==8)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 7, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  else if (j==5)
	    {
	      fitDSCB->SetParameters(1, 3, 1, 3, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	    }
	  
	  else if (j==0 || j==6 || j==2)
	    {
	      fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	      
	    }
	  
	  else fitDSCB->SetParameters(1, 2, 1, 2, hist_avgM_nom[j][channel]->GetMean(), hist_avgM_nom[j][channel]->GetRMS(), 1);
	}
	    
      fitDSCB->SetParNames ("alpha_{low}","alpha_{high}","n_{low}", "n_{high}", "mean", "sigma", "Norm");
      hist_avgM_nom[j][channel]->Fit(fitDSCB, "", "", leftRangeFit[j],rightRangeFit[j]);
      hist_avgM_nom[j][channel]->GetXaxis()->SetRangeUser(leftRangeHistX[j], rightRangeHistX[j]);
      
      Mean[j][channel] = fitDSCB->GetParameter(4);
      Sigma[j][channel]= fitDSCB->GetParameter(5);
      //Integral[j][channel] = fit->Integral(leftRangeHistX[j], rightRangeHistX[j]);
      err_Mean[j][channel] = fitDSCB->GetParError(4);
      err_Sigma[j][channel] = fitDSCB->GetParError(5);
      IntegralReco[j][channel] = hist_avgM_nom[j][channel]->Integral();
      //IntegralReco[j][channel] = hist_avgM_nom[j][channel]->GetEntries();
      maxFitResult = fitDSCB->GetMaximum();
      maxHistY = hist_avgM_nom[j][channel]->GetMaximum();
      if (channel ==0)
	{
	  if (maxFitResult >= maxHistY)
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.4*maxFitResult); // for at truth level with all channel, add 0.1
	    }
	  else
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.4*maxHistY);
	    }
	}
      else if (channel ==1)
	{
	  if (maxFitResult >= maxHistY)
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.4*maxFitResult); // for at truth level with all channel, add 0.1
	    }
	  else
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.4*maxHistY);
	    }
	}
      else if (channel ==2)
	{
	  if (maxFitResult >= maxHistY)
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.5*maxFitResult); // for at truth level with all channel, add 0.1
	    }
	  else
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.5*maxHistY);
	    }
	}
      
      else if (channel ==3)
	{
	  if (maxFitResult >= maxHistY)
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxFitResult+0.4*maxFitResult); // for at truth level with all channel, add 0.1
	    }
	  else
	    {
	      hist_avgM_nom[j][channel]->GetYaxis()->SetRangeUser(0., maxHistY+0.4*maxHistY);
	    }
	}
      
      hist_avgM_nom[j][channel]->Draw("func");
     
      thePad->Print(TString::Format("plots/hist_avgMll_%s_%s.pdf",l_name[j], i_name[channel]));
    }
}

void graph_integral_mZd()
  
{
  TMultiGraph  *mg  = new TMultiGraph();
  Double_t mass_mZd[6]  = {20,50,65,80,150,250};
  Double_t integral_mZd[6]  = {Integral[0][channel],Integral[2][channel],Integral[3][channel],Integral[5][channel],Integral[7][channel],Integral[8][channel]};
  TGraphErrors *gr_integral_mZd = new TGraphErrors(6,mass_mZd,integral_mZd,0,0);
  // TSpline3 *i_mZd = new TSpline3("grmZd",gr_integral_mZd);
  gr_integral_mZd->SetLineColor(kRed);
  gr_integral_mZd->Draw("apl");
  mg->Add(gr_integral_mZd);
  Double_t integral_reco_mZd[6]  = {IntegralReco[0][channel],IntegralReco[2][channel],IntegralReco[3][channel],IntegralReco[5][channel],IntegralReco[7][channel],IntegralReco[8][channel]};
  TGraphErrors *gr_integral_reco_mZd = new TGraphErrors(6,mass_mZd,integral_reco_mZd,0,0);
  // TSpline3 *r_mZd = new TSpline3("grmZd",gr_integral_reco_mZd);
  gr_integral_reco_mZd->SetTitle("");
  gr_integral_reco_mZd->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  gr_integral_reco_mZd->GetYaxis()->SetTitle("integral");
  gr_integral_reco_mZd->SetLineColor(kBlue);
  mg->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  mg->GetYaxis()->SetTitle("integral");
  mg->Add(gr_integral_reco_mZd);
  int n = gr_integral_reco_mZd->GetN();
  double* y = gr_integral_reco_mZd->GetY();
  int locmax = TMath::LocMax(n,y);
  double tmax = y[locmax]*10000;  
  mg->GetYaxis()->SetRangeUser(0.1, tmax);
  c1->SetLogy();
  mg->Draw("acp");
  //gr_integral_reco_mZd->Draw("acp");
  auto legend1 = new TLegend(0.2,0.7,0.3,0.8);
  legend1->SetTextFont(42);
  legend1->SetTextSize(0.03);
  legend1->SetBorderSize(0);
  legend1->AddEntry(gr_integral_reco_mZd,"Integral from reco","lf");
  legend1->AddEntry(gr_integral_mZd,"Integral from fit","lf");
  legend1->Draw();
  legend1->Draw();
  thePad->Print(TString::Format("plots/hist_integral_mZd_%s.pdf", i_name[channel]));
  
    for (int i =0; i<18; i++)
    {
      //IntOfRecoHist_mZd[i] = std::round(gr_integral_mZd->Eval(mZd_gen[i]));
      IntOfRecoHist_mZd[i] = std::round(gr_integral_reco_mZd->Eval(mZd_gen[i]));
    
    }
    /*  //cout << "IntOfRecoHist_mZd[0]" << gr_integral_mZd->Eval(70) << endl;
    cout << "count_reco_mZd[0] = " << IntegralReco[0][channel] << endl;
    cout << "count_reco_mZd[2] = " << IntegralReco[2][channel] << endl;
    cout << "count_reco_mZd[3] = " << IntegralReco[3][channel] << endl;
    cout << "count_reco_mZd[5] = " << IntegralReco[5][channel] << endl;
    cout << "count_reco_mZd[6] = " << IntegralReco[6][channel] << endl;
    cout << "count_reco_mZd[7] = " << IntegralReco[7][channel] << endl;
    cout << "count_reco_mZd[8] = " << IntegralReco[8][channel] << endl;

    //cout << "IntOfRecoHist_mZd[0]" << gr_integral_reco_mZd->Eval(70) << endl;
    cout << "IntOfRecoHist_mZd[0]" << IntOfRecoHist_mZd[0] << endl;

    //cout << "count_fit_mZd[0] = " << Integral[0][channel] << endl;
    cout << "count_fit_mZd[2] = " << Integral[2][channel] << endl;
    cout << "count_fit_mZd[3] = " << Integral[3][channel] << endl;
    cout << "count_fit_mZd[5] = " << Integral[5][channel] << endl;
    cout << "count_fit_mZd[6] = " << Integral[6][channel] << endl;
    cout << "count_fit_mZd[7] = " << Integral[7][channel] << endl;
    cout << "count_fit_mZd[8] = " << Integral[8][channel] << endl;*/
    

}

void graph_sigma_mZd()

{
  // c1->Divide(2,1);
  c1->SetLogy(0);
  Double_t mass_mZd[6]  = {20,50,65,80,150,250};
  Double_t sigma_mZd[6]  = {Sigma[0][channel],Sigma[2][channel],Sigma[3][channel],Sigma[5][channel],Sigma[7][channel],Sigma[8][channel]};
  Double_t sigma_error_mZd[6] = {err_Sigma[0][channel],err_Sigma[2][channel],err_Sigma[3][channel],err_Sigma[5][channel], err_Sigma[7][channel],err_Sigma[8][channel]};
  TGraphErrors *gr_sigma_mZd = new TGraphErrors(6,mass_mZd,sigma_mZd,0,sigma_error_mZd);
  TSpline3 *s_mZd = new TSpline3("grmZd",gr_sigma_mZd);
  
  gr_sigma_mZd->SetTitle("");
  gr_sigma_mZd->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  gr_sigma_mZd->GetYaxis()->SetTitle("Fit param #sigma");
  // c1->cd(1);
  gr_sigma_mZd->Fit("pol1","","",15,255);
  gr_sigma_mZd->GetXaxis()->SetRangeUser(0, 300);
  gr_sigma_mZd->GetYaxis()->SetRangeUser(0, 5);
  gr_sigma_mZd->SetMarkerSize(1);
  gr_sigma_mZd->SetMarkerStyle(3);
  gr_sigma_mZd->Draw("APE");
  gr_sigma_mZd->SetMarkerSize(1);
  gr_sigma_mZd->SetMarkerStyle(8);
  s_mZd->SetLineColor(kRed);
  thePad->Print(TString::Format("plots/hist_sigma_mZd_%s.pdf", i_name[channel]));
    for (int i =0; i<18; i++)
    {
           sigma_gen_mZd[i] = gr_sigma_mZd->GetFunction("pol1")->Eval(mZd_gen[i]);
	   //cout << "*********sigma_gen_mZd[0]******* = " << sigma_gen_mZd[0] << endl;
    }
    //cout << "*********sigma_gen_mZd[0]******* = " << sigma_gen_mZd[0] << endl;
}
void graph_mean_mZd()
{

Double_t mass_mZd[6]  = {20,50,65,80,150,250};
  Double_t mean_mZd[6]  = {Mean[0][channel],Mean[2][channel],Mean[3][channel],Mean[5][channel],Mean[7][channel],Mean[8][channel]};
  Double_t mean_error_mZd[6] = {err_Mean[0][channel],err_Mean[2][channel],err_Mean[3][channel],err_Mean[5][channel],err_Mean[7][channel],err_Mean[8][channel]};
  TGraphErrors *gr_mean_mZd = new TGraphErrors(6,mass_mZd,mean_mZd,0,mean_error_mZd);
   
  gr_mean_mZd->SetTitle("");
  gr_mean_mZd->GetXaxis()->SetTitle("m_{Zd} [GeV]");
  gr_mean_mZd->GetYaxis()->SetTitle("Fit param #mu");
  // c1->cd(1);
  gr_mean_mZd->Fit("pol1","","",20,250);
  gr_mean_mZd->GetXaxis()->SetRangeUser(0, 300);
  gr_mean_mZd->GetYaxis()->SetRangeUser(0, 400);
  gr_mean_mZd->Draw("APE");
  gr_mean_mZd->SetMarkerSize(1);
  gr_mean_mZd->SetMarkerStyle(8);
  thePad->Print(TString::Format("plots/hist_mean_mZd_%s.pdf", i_name[channel]));
    for (int i =0; i<18; i++)
    {
           mean_gen_mZd[i]  = gr_mean_mZd->GetFunction("pol1")->Eval(mZd_gen[i]);
	   cout << "*********mean_gen_mZd[0]******** =  " << mean_gen_mZd[0] << endl;
    }
   
}

void Generating_mZd_hist()
{
   for (int i_gen = 0; i_gen < 18; i_gen++)
    {
      rand_DSCB_mZd->SetParameters(5, 5, 5, 5, mean_gen_mZd[i_gen], sigma_gen_mZd[i_gen], 1);
      hGen_mZd[i_gen][channel]->FillRandom("rand_DSCB_mZd", IntOfRecoHist_mZd[i_gen]);
    }

  /* for (int i_gen = 0; i_gen < 18; i_gen++)
	{
	  for (int j = 0; j < IntOfRecoHist_mZd[i_gen]; j++)
	  //for (int j = 0; j < 281; j++)
	   
	    {
	     
	    
	      rand_DSCB_mZd->SetParameters(1, 1, 1, 1, mean_gen_mZd[i_gen], sigma_gen_mZd[i_gen], 1);
	      M_Z_d_i = rand_DSCB_mZd->GetRandom();
	      hGen_mZd[i_gen][channel]->Fill(M_Z_d_i);
	    }
	}*/
  
}


void compare_mZdGen_mZdReco()
{
  c1->SetLogy(0);
  // double scale = 1/hGen_mZd[0][channel]->Integral();
  // hGen_mZd[0][channel]->Scale(scale);
  // double scale70 = 1/hist_avgM_nom[4][channel]->Integral();
  //hist_avgM_nom[4][channel]->Scale(scale70);
  //hGen_mZd[0][channel]->Scale(0.1);
  // hGen_mZd[0][channel]->GetXaxis()->SetRangeUser(0,150);
  //hist_avgM_nom[4][channel]->GetXaxis()->SetRangeUser(0,150);
  hGen_mZd[0][channel]->SetLineColor(2);
  hist_avgM_nom[4][channel]->SetLineColor(4);
  c1->Clear();
  gStyle->SetOptFit(0);
  gStyle->SetOptStat(0);
  hGen_mZd[0][channel]->Draw("hist");
  gPad->Update();
  hist_avgM_nom[4][channel]->Draw("hist sames");
  auto legend = new TLegend(0.2,0.5,0.3,0.8);
  legend->SetTextFont(42);
  legend->SetTextSize(0.03);
  legend->SetBorderSize(0);
  legend->AddEntry(hist_avgM_nom[4][channel],"reco signal","lf");
  legend->AddEntry(hGen_mZd[0][channel],"Generated signal","lf");
  //legend->SetX1NDC(.5); //new x start position
  //legend->SetX2NDC(.7);
  legend->Draw();
  thePad->Print(TString::Format("plots/hGen_mZd_70_%s.pdf", i_name[channel]));
}

void SaveInNomDirect()

{

 for (int i_gen = 0; i_gen < 18; i_gen++)
	{
	  QCD_scale_up[i_gen][channel]   = (IntOfRecoHist_mZd[i_gen] + IntOfRecoHist_mZd[i_gen]*QCD_uncert_up[i_gen])/IntOfRecoHist_mZd[i_gen];
	  QCD_scale_down[i_gen][channel] = (IntOfRecoHist_mZd[i_gen] + IntOfRecoHist_mZd[i_gen]*QCD_uncert_down[i_gen])/IntOfRecoHist_mZd[i_gen];
	  
	  PDF_scale_up[i_gen][channel]   = (IntOfRecoHist_mZd[i_gen] + IntOfRecoHist_mZd[i_gen]*PDF_uncert_up[i_gen])/IntOfRecoHist_mZd[i_gen];
	  PDF_scale_down[i_gen][channel] = (IntOfRecoHist_mZd[i_gen] + IntOfRecoHist_mZd[i_gen]*PDF_uncert_down[i_gen])/IntOfRecoHist_mZd[i_gen];
	  
	  file_saveFitHist =  new TFile(TString::Format("BRscaled_gaussiansignal_%schannel_%s.root",i_name[channel], c_mZdmS[i_gen]),"recreate");
	  sig = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig->SetName("sig");
	  sig_up = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig_up->SetName("sig_up");
	  sig_down = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig_down->SetName("sig_down");
	  myDirect = file_saveFitHist->mkdir("Nominal");
	  file_saveFitHist->cd("Nominal");
	  sig->Write();
	  myDirect = file_saveFitHist->mkdir("STAT");
	  file_saveFitHist->cd("STAT");
	  sig->Write();
	  myDirect = file_saveFitHist->mkdir("LUMI");
	  file_saveFitHist->cd("LUMI");
	  sig_up->Write();
	  sig_down->Write();
	  myDirect = file_saveFitHist->mkdir("QCDSCALE");
	  file_saveFitHist->cd("QCDSCALE");
	  sig_up->Scale(QCD_scale_up[i_gen][channel]);
	  sig_up->Write();
	  sig_down->Scale(QCD_scale_down[i_gen][channel]);
	  sig_down->Write();
	  myDirect = file_saveFitHist->mkdir("PDF");
	  file_saveFitHist->cd("PDF");
	  sig_up->Scale(PDF_scale_up[i_gen][channel]/QCD_scale_up[i_gen][channel]);
	  sig_up->Write();
	  sig_down->Scale(PDF_scale_down[i_gen][channel]/QCD_scale_up[i_gen][channel]);
	  sig_down->Write();
	  sig->Reset();
	  sig_up->Reset();
	  sig_down->Reset();
	  file_saveFitHist->Close();
	  
	}
     
}

void SaveOtherSystDirect()
{
for (int i_gen = 0; i_gen < 18; i_gen++)
	{

	  
	  file_saveFitHist =  new TFile(TString::Format("BRscaled_gaussiansignal_%schannel_%s.root",i_name[channel], c_mZdmS[i_gen]),"update");
	  if (i_var ==0)  myDirect = file_saveFitHist->mkdir(TString::Format("%s",c_wSyst[i_Nwsyst]));
	  else file_saveFitHist->cd(TString::Format("%s",c_wSyst[i_Nwsyst])); 
	  // for (int i_var = 0; i_var < 2; i_var++)
	    // {
	  sig = (TH1F*)hGen_mZd[i_gen][channel]->Clone(TString::Format("hGen_mZd_%s_%s", i_name[channel], c_mZd[i_gen]));
	  sig->SetName(TString::Format("sig_%s",c_vSyst[i_var]));
	  file_saveFitHist->cd(TString::Format("%s",c_wSyst[i_Nwsyst]));
	  sig->Write();
	  sig->Reset();
	  // }
	  file_saveFitHist->Close();
	}
}
